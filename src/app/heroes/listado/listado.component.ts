import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html'
})
export class ListadoComponent {
  heroes: string[]=['Spiderman','Ironman','Thor','Hulk'];
  heroesBorrado: string='';

  borrarHeroe():void{
    const heroeBorrado=this.heroes.pop();
    if(heroeBorrado){
      this.heroesBorrado=heroeBorrado||'';
    }
    
  }
}
